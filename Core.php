<?php

namespace dautkom\docsis;
use dautkom\netsnmp;


/**
 * Class Core
 * @package dautkom\docsis
 */
abstract class Core
{

    /**
     * SNMP instance
     * @var netsnmp\NetSNMP
     */
    protected static $snmp;

    /**
     * Connection status flag
     * @var bool
     */
    protected static $connected = false;

    /**
     * Device information from ini file.
     * @var array
     */
    protected static $device = [];


    /**
     * Wrapper for snmpget(). Fetch an SNMP object. Accepts both strings and arrays as OIDs.
     * When $oid is an array and keys in results will be taken exactly as in object_id.
     *
     * @param  array|string $oid SNMP object identifier
     * @return mixed
     */
    protected function get($oid)
    {

        if( self::$connected === false ) {
            trigger_error("Unable to send SNMP query while not connected to the device", E_USER_ERROR);
            return null;
        }

        try {
            $snmp_get = self::$snmp->get($oid);
        }
        catch (\Throwable $t) {
            $snmp_get = false;
        }

        return $snmp_get;

    }


    /**
     * Wrapper for snmpwalk() and snmprealwalk(). Fetch SNMP object subtree
     *
     * @param  string $oid  SNMP object identifier representing root of subtree to be fetched
     * @param  bool   $real [optional] By default full OID notation is used for keys in output array. If set to <b>TRUE</b> subtree prefix will be removed from keys leaving only suffix of object_id.
     * @return array
     */
    protected function walk(string $oid, bool $real = false): array
    {

        if( self::$connected === false ) {
            trigger_error("Unable to send SNMP query while not connected to the device", E_USER_ERROR);
            return null;
        }

        try {
            $snmp_walk = self::$snmp->walk($oid, $real);
        }
        catch (\Throwable $t) {
            $snmp_walk = [];
        }

        return $snmp_walk;

    }


    /**
     * Wrapper for snmpset(). Set the value of an SNMP object. Accepts both strings and arrays as arguments
     *
     * @param  array|string $oid   SNMP object identifier
     * @param  array|string $type  content type literal
     * @param  array|string $value writable data
     * @return bool
     */
    public function set($oid, $type, $value): bool
    {

        if( self::$connected === false ) {
            trigger_error("Unable to send SNMP query while not connected to the device", E_USER_ERROR);
            return null;
        }

        return self::$snmp->set($oid, $type, $value);

    }

}
