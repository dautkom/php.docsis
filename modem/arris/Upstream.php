<?php

namespace dautkom\docsis\modem\arris;


/**
 * Methods for work related to cable modem upstream channel params and settings on Arris cable modems
 * @package dautkom\docsis\modem\arris
 */
class Upstream extends \dautkom\docsis\modem\Upstream
{

    /**
     * It's impossible to set upstream channel id for Arris Touchstone devices.
     *
     * @param  int $id channel ID
     * @return bool
     */
    public function setChannelId(int $id): bool 
    {
        return false;
    }

}