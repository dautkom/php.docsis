<?php

namespace dautkom\docsis\modem\arris;
use dautkom\docsis\{
    Modem, modem\Fdb, modem\Ipif, modem\Log, modem\Rf
};


/**
 * Common methods for Arris cable modems
 * @package dautkom\docsis\modem\arris
 */
class Common extends \dautkom\docsis\modem\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->rf         = new Rf();
        $this->fdb        = new Fdb();
        $this->log        = new Log();
        $this->ipif       = new Ipif();
        $this->upstream   = new Upstream();
        $this->downstream = new Downstream();

    }


    /**
     * Retrieve standby mode
     *
     * Return values:
     * 0: n/a
     * 1: in stanby
     * 2: not in standby
     *
     * @return int
     */
    public function getStandby(): int
    {

        $mode = $this->get( parent::$device["standby"] );

        if( $mode > 0 && $mode < 5 ) {
            return ($mode % 2) + 1;
        }

        return 0;

    }

}
