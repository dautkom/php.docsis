<?php

namespace dautkom\docsis\modem\arris;


/**
 * Methods for work related to cable modem downstream channel params and settings on Arris cable modems
 * @package dautkom\docsis\modem\arris
 */
class Downstream extends \dautkom\docsis\modem\Downstream
{

    /**
     * It's impossible to set downstream frequency center for Arris Touchstone devices.
     *
     * @param  string $frequency frequency in Hz
     * @return bool
     */
    public function setFrequency(string $frequency): bool
    {
        return false;
    }
    
}
