<?php

namespace dautkom\docsis\modem;
use dautkom\docsis\Modem;


/**
 * Methods for work related to cable modem upstream channel params and settings
 * @package dautkom\docsis\modem
 */
class Upstream extends Modem
{

    /**
     * The operational transmit power for the attached upstream channel.
     *
     * @return string
     */
    public function getPowerLevel(): string
    {
        return $this->get( parent::$device["docsIfCmStatusTxPower"] );
    }


    /**
     * The bandwidth of this upstream interface.  This object
     * returns 0 if the interface width is undefined or unknown.
     * Minimum permitted interface width is currently 200,000 Hz.
     *
     * @return string
     */
    public function getChannelWidth(): string
    {
        return $this->get( parent::$device["docsIfUpChannelWidth"] );
    }


    /**
     * The CMTS identification of the upstream channel.
     *
     * @return string
     */
    public function getChannelId(): string
    {
        return $this->get( parent::$device["docsIfUpChannelId"] );
    }


    /**
     * Set upstream channel
     *
     * @param  int $id channel ID
     * @return bool
     */
    public function setChannelId(int $id): bool
    {
        return $this->set( parent::$device["docsIfUpChannelId"], 'i', intval($id));
    }


    /**
     * The center of the frequency band associated with this
     * upstream interface.  This object returns 0 if the frequency
     * is undefined or unknown.  Minimum permitted upstream
     * frequency is 5,000,000 Hz for current technology.
     *
     * @return string
     */
    public function getFrequency(): string
    {
        return $this->get( parent::$device["docsIfUpChannelFrequency"] );
    }

}
