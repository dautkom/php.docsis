<?php

namespace dautkom\docsis\modem;
use dautkom\docsis\Modem;


/**
 * Methods for retrieving cable modem log entries
 * @package dautkom\docsis\modem
 */
class Log extends Modem
{

    /**
     * Retrieve log data from cable modem.
     * Data returned in human readable state.
     *
     * @return array
     */
    public function getLog(): array
    {

        $log      = [];
        $raw_log  = $this->walk('.1.3.6.1.2.1.69.1.5.8.1.7');
        $raw_time = $this->walk('.1.3.6.1.2.1.69.1.5.8.1.3');

        if( !empty($raw_time) && !empty($raw_log) && count($raw_log) == count($raw_time) ) {

            foreach( $raw_log as $ix => $data ) {

                $ix   = str_replace( '.69.1.5.8.1.7', '.69.1.5.8.1.3', $ix );
                $time = trim($raw_time[$ix]);
                $time = explode(' ', $time);
                $time = hexdec($time[0] . $time[1]) . "-" . hexdec($time[2]) . "-" .hexdec($time[3]) . " " .hexdec($time[4]) . ":" . hexdec($time[5]) . ":" . hexdec($time[6]);
                $time = strtotime($time);

                $log[$time] = $data;

            }

        }

        return $log;

    }

}
