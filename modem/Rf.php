<?php

namespace dautkom\docsis\modem;
use dautkom\docsis\Modem;


/**
 * Methods for work related to cable modem radiofrequency params and settings
 * @package dautkom\docsis\modem
 */
class Rf extends Modem
{

    /**
     * Retrieve cable modem operating mode.
     * Non-intuitive values (2 for 1 and 1 for 2) are left due to stored data format.
     * Unnecessary value translation is considered as a bad practice.
     * 
     * Return values:
     * 0: n/a
     * 1: docsis 2
     * 2: docsis 1
     *
     * @return int
     */
    public function getD2Mode(): int
    {
        $raw = $this->get( parent::$device["cmDocsis20Capable"] );
        return intval($raw);
    }


    /**
     * Sets cable modem operating mode
     * Non-intuitive values (2 for 1 and 1 for 2) are left due to stored data format. 
     * Unnecessary value translation is considered as a bad practice.
     * 
     * Param values:
     * 1: docsis 2
     * 2: docsis 1
     *
     * @param  int $mode Docsis operating mode
     * @throws \UnexpectedValueException
     * @return bool
     */
    public function setD2Mode( int $mode ): bool
    {
        
        if( $mode > 2 || $mode < 1 ) {
            throw new \UnexpectedValueException('Docsis operating mode must be either 1 or 2');
        }
        
        return $this->set(parent::$device["cmDocsis20Capable"], 'i', intval($mode));
        
    }


    /**
     * Get information about data flow errors from
     * docsIfSignalQualityEntry SNMP tree
     *
     * @return array
     */
    public function getErrors(): array
    {

        $data = $this->get([
            '.1.3.6.1.2.1.10.127.1.1.4.1.2.3',
            '.1.3.6.1.2.1.10.127.1.1.4.1.3.3',
            '.1.3.6.1.2.1.10.127.1.1.4.1.4.3'
        ]);
        
        return [
            'data'          => $data['.1.3.6.1.2.1.10.127.1.1.4.1.2.3'],
            'corrected'     => $data['.1.3.6.1.2.1.10.127.1.1.4.1.3.3'],
            'uncorrectable' => $data['.1.3.6.1.2.1.10.127.1.1.4.1.4.3']
        ];

    }


    /**
     * Number of times the cable modem reset or initialized this interface.
     *
     * @return string
     */
    public function getCMResetAmount(): string
    {
        return $this->get('.1.3.6.1.2.1.10.127.1.2.2.1.4.2');
    }


    /**
     * Number of times the cable modem lost synchronization with the downstream channel.
     *
     * @return string
     */
    public function getCMLostSyncAmount(): string
    {
        return $this->get('.1.3.6.1.2.1.10.127.1.2.2.1.5.2');
    }


    /**
     * Number of times the ranging process was aborted by the CMTS.
     *
     * @return string
     */
    public function getCMRangingAbortAmount(): string
    {
        return $this->get('.1.3.6.1.2.1.10.127.1.2.2.1.14.2');
    }

}
