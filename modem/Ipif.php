<?php

namespace dautkom\docsis\modem;
use dautkom\docsis\Modem;


/**
 * Methods for work related to cable modem IP interfaces params and settings
 * @package dautkom\docsis\modem
 */
class Ipif extends Modem
{

    /**
     * Retrieve amount of sent/recieved packets on port.
     * Data is grouped by interface.
     *
     * If modem is plugged in via USB, there will be 0 on eth-in and some false data on eth-out.
     * It's a known bug/feature and should be fixed on a client side if necessary
     *
     * @return array
     */
    public function getDataFlow(): array
    {

        $data = $this->get([
            '1.3.6.1.2.1.2.2.1.10.1', '1.3.6.1.2.1.2.2.1.16.1',
            '1.3.6.1.2.1.2.2.1.10.5', '1.3.6.1.2.1.2.2.1.16.5'
        ]);

        return [
            'eth' => [
                'in'  => $data['1.3.6.1.2.1.2.2.1.10.1'],
                'out' => $data['1.3.6.1.2.1.2.2.1.16.1'],
            ],
            'usb' => [
                'in'  => $data['1.3.6.1.2.1.2.2.1.10.5'],
                'out' => $data['1.3.6.1.2.1.2.2.1.16.5'],
            ],
        ];

    }


    /**
     * Retrieve sum of errors and discards on both interfaces
     *
     * @return array
     */
    public function getErrors(): array
    {

        $data = $this->get([
            '1.3.6.1.2.1.2.2.1.14.1', '1.3.6.1.2.1.2.2.1.13.1',
            '1.3.6.1.2.1.2.2.1.20.1', '1.3.6.1.2.1.2.2.1.19.1',
            '1.3.6.1.2.1.2.2.1.14.5', '1.3.6.1.2.1.2.2.1.13.5',
            '1.3.6.1.2.1.2.2.1.20.5', '1.3.6.1.2.1.2.2.1.19.5'
        ]);

        $data = array_map('intval', $data);

        return [
            'eth' => [
                'in'  => $data['1.3.6.1.2.1.2.2.1.14.1'] + $data['1.3.6.1.2.1.2.2.1.13.1'],
                'out' => $data['1.3.6.1.2.1.2.2.1.20.1'] + $data['1.3.6.1.2.1.2.2.1.19.1'],
            ],
            'usb' => [
                'in'  => $data['1.3.6.1.2.1.2.2.1.14.5'] + $data['1.3.6.1.2.1.2.2.1.13.5'],
                'out' => $data['1.3.6.1.2.1.2.2.1.20.5'] + $data['1.3.6.1.2.1.2.2.1.19.5'],
            ],
        ];

    }


    /**
     * Retrieve port state.
     * 
     * Return values:
     * 0: no link
     * 1: has link
     *
     * $this->snmpQuery('.1.3.6.1.2.1.2.2.1.8.X') returns 1 for link up and 2 or 5 for link down
     *
     * @return array
     */
    public function getPortState(): array
    {

        $raw  = $this->get(['.1.3.6.1.2.1.2.2.1.8.1', '.1.3.6.1.2.1.2.2.1.8.5']);
        $data = [
            'eth' => preg_replace('/[^\d]/', '', $raw['.1.3.6.1.2.1.2.2.1.8.1']),
            'usb' => preg_replace('/[^\d]/', '', $raw['.1.3.6.1.2.1.2.2.1.8.5']),
        ];

        return array_map(function($val) { return intval(!boolval(intval($val) - 1)); }, $data);

    }


    /**
     * Retrieve amount of established outgoing connections on 25 port
     *
     * @return string
     */
    public function get25PortMatches(): string
    {
        return strval($this->get('.1.3.6.1.2.1.69.1.6.4.1.16.4'));
    }


    /**
     * Retrieve if 25 port is blocked on cable modem.
     * 
     * Return values:
     * 0: n/a
     * 1: blocked
     * 2: non-blocked
     *
     * @return int
     */
    public function is25PortBlocked(): int
    {

        $mode = $this->get('.1.3.6.1.2.1.69.1.6.4.1.3.4');

        if( $mode ) {
            $mode = preg_replace('/[^\d]/', '', $mode);
        }

        return intval($mode);
    }


    /**
     * Set DSCP mark to 1
     * Can not be set to 0. Reset modem to unset.
     *
     * @return bool
     */
    public function set25PortBlocked(): bool
    {
        return $this->set('.1.3.6.1.2.1.69.1.6.4.1.3.4', 'i', 1);
    }


    /**
     * Retrieve access attempts to TFTP via ETH
     *
     * @return string
     */
    public function getTFTPAccessAttempts(): string
    {
        return $this->get('.1.3.6.1.2.1.69.1.6.4.1.16.3');
    }

}
