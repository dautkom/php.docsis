<?php

namespace dautkom\docsis\modem;
use dautkom\docsis\Modem;


/**
 * Methods for retrieving cable modem FDB table 
 * @package dautkom\docsis\modem
 */
class Fdb extends Modem
{

    /**
     * Retrieve FDB table
     *
     * @return array
     */
    public function getFdb(): array
    {

        $result = [];

        // Get MAC-address table
        $fdb_table  = $this->walk('.1.3.6.1.2.1.17.4.3.1.1');

        // Get MAC-address statuses
        $fdb_status = $this->walk('.1.3.6.1.2.1.17.4.3.1.3');

        if( $fdb_table && $fdb_status ) {

            // Loop through statuses
            foreach( $fdb_status as $oid => $status ) {

                // If status is 'learned'
                if( $status == 3 ) {

                    $oid = str_replace('2.17.4.3.1.3', '2.17.4.3.1.1', $oid); // create array index for $fdb_table
                    $fdb = $fdb_table[$oid];                                  // get learned MAC from $fdb_table
                    $fdb = preg_replace('/[^a-f0-9]/i', '', $fdb);            // clear it from obsolete symbols

                    // If MAC is valid
                    if( preg_match('/[0-9a-f]{12}/i', $fdb) && !preg_match('/^0000/m', $fdb) ) {
                        $result[] = $fdb;
                    }

                }

            }

        }

        return $result;

    }

}
