<?php

namespace dautkom\docsis\modem;
use dautkom\docsis\modem;


/**
 * Cable modem common methods
 * @package dautkom\docsis\modem
 *
 * @property Rf $rf
 * @property Fdb $fdb
 * @property Log $log
 * @property Ipif $ipif
 * @property Upstream $upstream
 * @property Downstream $downstream
 */
abstract class Common extends Modem
{

    /** @var Rf */
    public $rf;

    /** @var Fdb */
    public $fdb;

    /** @var Log */
    public $log;

    /** @var Ipif */
    public $ipif;

    /** @var Upstream */
    public $upstream;

    /** @var Downstream */
    public $downstream;


    /**
     * Load ini file
     * @ignore
     */
    public function __construct()
    {
        $this->loadIni();
    }

    
    /**
     * Retrieve cable modem description
     *
     * @return string
     */
    public function getSysDescr(): string
    {
        $raw = $this->get('.1.3.6.1.2.1.1.1.0');
        return htmlentities($raw);
    }
    

    /**
     * Retrieve cable modem uptime from sysUptime.
     *
     * You can filter timeticks from the result and convert it to necessary format
     * 1 timetick = 1/100 second
     *
     * @return string
     */
    public function getUptime(): string
    {
        return $this->get('.1.3.6.1.2.1.1.3.0');
    }


    /**
     * Retrieve modem serial number
     *
     * @return string
     */
    public function getSerialNumber(): string
    {
        return $this->get('.1.3.6.1.2.1.69.1.1.4.0');
    }


    /**
     * Get cable modem current configuration file
     *
     * @return string
     */
    public function getConfig(): string
    {
        return $this->get('.1.3.6.1.2.1.69.1.4.5.0');
    }


    /**
     * Reboot cable modem
     *
     * @return bool
     */
    public function reboot(): bool
    {
        return $this->set('.1.3.6.1.3.83.1.1.3.0', 'i', 1);
    }


    /**
     * Retrieve standby mode. Due to implementation differences in certain
     * models, method declared as abstract and must be redefined for each vendor and/or model.
     *
     * Return values: 
     * 0: n/a
     * 1: in stanby
     * 2: not in standby
     * 
     * @abstract
     * @return int
     */
    abstract public function getStandby(): int;

}
