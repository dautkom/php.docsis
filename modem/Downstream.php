<?php

namespace dautkom\docsis\modem;
use dautkom\docsis\Modem;


/**
 * Methods for work related to cable modem downstream channel params and settings
 * @package dautkom\docsis\modem
 */
class Downstream extends Modem
{

    /**
     * Signal/Noise ratio as perceived for this channel.
     * At the CM, this object  describes the Signal/Noise of the
     * downstream channel.
     *
     * @return string
     */
    public function getSNR(): string
    {
        return strval($this->get('.1.3.6.1.2.1.10.127.1.1.4.1.5.3'));
    }


    /**
     * Microreflections, including in-channel response as perceived on this interface, measured in dBc below
     * the signal level.
     * This object is not assumed to return an absolutely accurate value, but it gives a rough indication
     * of microreflections received on this interface. It is up to the implementer to provide information
     * as accurately as possible.
     *
     * @return string
     */
    public function getMicroreflections(): string
    {
        return strval($this->get('.1.3.6.1.2.1.10.127.1.1.4.1.6.3'));
    }


    /**
     * CMTS  identification of the downstream channel within this particular MAC interface.
     * if the interface is down, the object returns the most
     * current value.  If the downstream channel ID is unknown,
     * this object returns a value of 0.
     *
     * @return string
     */
    public function getChannelId(): string
    {
        return strval($this->get(parent::$device["docsIfDownChannelId"]));
    }


    /**
     * Received power level.
     * If the interface is down, this object either returns
     * the configured value (CMTS), the most current value (CM)
     * or the value of 0.
     *
     * @return string
     */
    public function getPowerLevel(): string
    {
        return strval($this->get(parent::$device["docsIfDownChannelPower"]));
    }


    /**
     * The center of the downstream frequency associated with
     * this channel.  This object will return the current tuner
     * frequency.  If a CMTS provides IF output, this object
     * will return 0, unless this CMTS is in control of the
     * final downstream frequency.
     *
     * @return string
     */
    public function getFrequency(): string
    {
        return strval($this->get(parent::$device["docsIfDownChannelFrequency"]));
    }


    /**
     * Set downstream frequency center
     *
     * @param  string $frequency frequency in Hz
     * @return bool
     */
    public function setFrequency( string $frequency ): bool
    {
        return $this->set(parent::$device["docsIfDownChannelFrequency"], 'i', trim($frequency));
    }

}
