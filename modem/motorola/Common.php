<?php

namespace dautkom\docsis\modem\motorola;
use dautkom\docsis\{
    Modem, modem\Downstream, modem\Fdb, modem\Ipif, modem\Log, modem\Rf, modem\Upstream
};


/**
 * Common methods for Motorola cable modems
 * @package dautkom\docsis\modem\motorola
 */
class Common extends \dautkom\docsis\modem\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {

        parent::__construct();
        
        $this->rf         = new Rf();
        $this->fdb        = new Fdb();
        $this->log        = new Log();
        $this->ipif       = new Ipif();
        $this->upstream   = new Upstream();
        $this->downstream = new Downstream();
        
    }


    /**
     * Retrieve standby mode
     * 
     * Return values:
     * 0: n/a
     * 1: in stanby
     * 2: not in standby
     *
     * @return int
     */
    public function getStandby(): int
    {
        return intval( $this->get( parent::$device["standby"] ));
    }

}
