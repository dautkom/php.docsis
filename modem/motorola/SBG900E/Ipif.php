<?php

namespace dautkom\docsis\modem\motorola\SBG900E;


/**
 * Methods for work related to cable modem IP interfaces params and settings on Motorola SBG900E cable modems
 * @package dautkom\docsis\modem\motorola\SBG900E
 */
class Ipif extends \dautkom\docsis\modem\Ipif
{

    /**
     * Retrieve port state.
     *
     * Return values:
     * 0: no link
     * 1: has link
     *
     * $this->snmpQuery('.1.3.6.1.2.1.2.2.1.8.X') returns 1 for link up and 2 or 5 for link down
     *
     * @return array
     */
    public function getPortState(): array
    {

        $raw  = $this->get(['.1.3.6.1.2.1.2.2.1.8.1', '.1.3.6.1.2.1.2.2.1.8.5', '.1.3.6.1.2.1.2.2.1.8.6']);
        $data = [
            'eth'  => preg_replace('/[^\d]/', '', $raw['.1.3.6.1.2.1.2.2.1.8.1']),
            'usb'  => preg_replace('/[^\d]/', '', $raw['.1.3.6.1.2.1.2.2.1.8.5']),
            'wifi' => preg_replace('/[^\d]/', '', $raw['.1.3.6.1.2.1.2.2.1.8.6']),
        ];

        return array_map(function($val) { return intval(!boolval(intval($val) - 1)); }, $data);

    }


    /**
     * Retrieve amount of sent/recieved packets on port.
     * Data is grouped by interface.
     *
     * If modem is plugged in via USB, there will be 0 on eth-in and some false data on eth-out.
     * It's a known bug/feature and should be fixed on a client side if necessary
     *
     * @return array
     */
    public function getDataFlow(): array
    {

        $data = $this->get([
            '1.3.6.1.2.1.2.2.1.10.1', '1.3.6.1.2.1.2.2.1.16.1',
            '1.3.6.1.2.1.2.2.1.10.5', '1.3.6.1.2.1.2.2.1.16.5',
            '1.3.6.1.2.1.2.2.1.10.6', '1.3.6.1.2.1.2.2.1.16.6'
        ]);

        return [
            'eth' => [
                'in'  => $data['1.3.6.1.2.1.2.2.1.10.1'],
                'out' => $data['1.3.6.1.2.1.2.2.1.16.1'],
            ],
            'usb' => [
                'in'  => $data['1.3.6.1.2.1.2.2.1.10.5'],
                'out' => $data['1.3.6.1.2.1.2.2.1.16.5'],
            ],
            'wifi' => [
                'in'  => $data['1.3.6.1.2.1.2.2.1.10.6'],
                'out' => $data['1.3.6.1.2.1.2.2.1.16.6'],
            ],
        ];

    }


    /**
     * Retrieve sum of errors and discards on both interfaces
     *
     * @return array
     */
    public function getErrors(): array 
    {

        $data = $this->get([
            '1.3.6.1.2.1.2.2.1.14.1', '1.3.6.1.2.1.2.2.1.13.1',
            '1.3.6.1.2.1.2.2.1.20.1', '1.3.6.1.2.1.2.2.1.19.1',
            '1.3.6.1.2.1.2.2.1.14.5', '1.3.6.1.2.1.2.2.1.13.5',
            '1.3.6.1.2.1.2.2.1.20.5', '1.3.6.1.2.1.2.2.1.19.5',
            '1.3.6.1.2.1.2.2.1.14.6', '1.3.6.1.2.1.2.2.1.13.6',
            '1.3.6.1.2.1.2.2.1.20.6', '1.3.6.1.2.1.2.2.1.19.6'
        ]);

        $data = array_map('intval', $data);

        return [
            'eth' => [
                'in'  => $data['1.3.6.1.2.1.2.2.1.14.1'] + $data['1.3.6.1.2.1.2.2.1.13.1'],
                'out' => $data['1.3.6.1.2.1.2.2.1.20.1'] + $data['1.3.6.1.2.1.2.2.1.19.1'],
            ],
            'usb' => [
                'in'  => $data['1.3.6.1.2.1.2.2.1.14.5'] + $data['1.3.6.1.2.1.2.2.1.13.5'],
                'out' => $data['1.3.6.1.2.1.2.2.1.20.5'] + $data['1.3.6.1.2.1.2.2.1.19.5'],
            ],
            'wifi' => [
                'in'  => $data['1.3.6.1.2.1.2.2.1.14.6'] + $data['1.3.6.1.2.1.2.2.1.13.6'],
                'out' => $data['1.3.6.1.2.1.2.2.1.20.6'] + $data['1.3.6.1.2.1.2.2.1.19.6'],
            ],
        ];

    }

}
