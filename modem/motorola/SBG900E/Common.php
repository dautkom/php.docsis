<?php

namespace dautkom\docsis\modem\motorola\SBG900E;
use dautkom\docsis\{
    Modem, modem\Downstream, modem\Fdb, modem\Log, modem\Rf, modem\Upstream
};


/**
 * Common methods for Motorola SBG900E cable modems
 * @package dautkom\docsis\modem\motorola\SBG900E
 */
class Common extends \dautkom\docsis\modem\motorola\Common
{
    
    /** @noinspection PhpMissingParentConstructorInspection
     *  @ignore
     */
    public function __construct()
    {

        parent::__construct();

        $this->rf         = new Rf();
        $this->fdb        = new Fdb();
        $this->log        = new Log();
        $this->ipif       = new Ipif();
        $this->upstream   = new Upstream();
        $this->downstream = new Downstream();

    }
    
}
