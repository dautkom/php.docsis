<?php

namespace dautkom\docsis\cmts;
use dautkom\{
    docsis\Cmts
};


/**
 * CMTS common methods
 * @package dautkom\docsis\cmts
 *
 * @property Downstream $downstream
 * @property Upstream $upstream
 * @property Modems $modems
 *
 */
class Common extends Cmts
{

    /** @var Downstream */
    public $downstream;
    
    /** @var Upstream */
    public $upstream;
    
    /** @var Modems */
    public $modems;


    /**
     * Retrieve CMTS system description from sysDescr.
     *
     * @return string
     */
    public function getSysDescr(): string
    {
        $data = $this->get('.1.3.6.1.2.1.1.1.0');
        return htmlentities($data);
    }


    /**
     * Retrieve CMTS uptime from sysUptime.
     *
     * You can filter timeticks from the result and convert it to necessary format
     * 1 timetick = 1/100 second
     *
     * @return string
     */
    public function getUptime(): string
    {
        return $this->get('.1.3.6.1.2.1.1.3.0');
    }


    /**
     * Retrieve CMTS system name from sysName.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->get('.1.3.6.1.2.1.1.5.0');
    }


    /**
     * Retrieve CMTS system contact from sysContact.
     *
     * @return string
     */
    public function getContact(): string
    {
        return $this->get('.1.3.6.1.2.1.1.4.0');
    }


    /**
     * Retrieve CMTS system location from sysLocation.
     *
     * @return string
     */
    public function getLocation(): string
    {
        return $this->get('.1.3.6.1.2.1.1.6.0');
    }

}
