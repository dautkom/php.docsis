<?php

namespace dautkom\docsis\cmts;
use dautkom\docsis\Cmts;


/**
 * Methods for retrieving calbe modems data from CMTS 
 * @package dautkom\docsis\cmts
 */
class Modems extends Cmts
{

    /**
     * Retrieve all cable modems from CMTS.
     *
     * Example:
     * array(
     *   (int)index => array(
     *     'mac' => (string)modem MAC-address
     *     'up'  => (int)upstream channel internal index
     *   )
     * )
     *
     * To retrieve upstream channel data, use $cmts->upstream->getUpstreams()
     * resulting array and process it manually to 0.0, 0.1, 1.0 etc.
     *
     * @return array
     */
    public function getAll(): array
    {
        $res = [];
        $mac = $this->walk('.1.3.6.1.2.1.10.127.1.3.3.1.2');
        $up  = $this->walk('.1.3.6.1.2.1.10.127.1.3.3.1.5');

        array_walk($mac, function($v, $k) use(&$res, $up) {
            $k              = (int)substr($k, strrpos($k, '.') + 1);
            $res[$k]['mac'] = preg_replace('/[^0-9a-f]/i', '', $v);
            $res[$k]['up']  = intval($up["SNMPv2-SMI::transmission.127.1.3.3.1.5.$k"]);
        });

        return $res;

    }


    /**
     * Retrieve list of cable modems on a particular upstream channel.
     * To get list of upstreams, use $cmts->upstream->getUpstreams()
     *
     * @param  int $index upstream channel ID
     * @return array
     */
    public function getByUpstreamIndex(int $index): array
    {

        $res = [];
        $raw = $this->getAll();

        array_walk($raw, function($v, $k) use(&$res) {
            $res[$v['up']][] = [
                'mac' => $v['mac'],
                'id'  => $k,
            ];
        });

        return array_key_exists($index, $res) ? $res[$index] : [];

    }

}
