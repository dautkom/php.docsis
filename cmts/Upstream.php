<?php

namespace dautkom\docsis\cmts;
use dautkom\docsis\Cmts;


/**
 * Methods for retrieving CMTS upstream channels params and settings
 * @package dautkom\docsis\cmts
 */
class Upstream extends Cmts
{

    /**
     * Retrieve all upstreams IDs from docsIfUpChannelId. Example:
     * array(
     *   (int)id => (string)index,
     *   ...
     * )
     *
     * @return array
     */
    public function getUpstreams(): array
    {

        $res = [];
        $raw = $this->walk('.1.3.6.1.2.1.10.127.1.1.2.1.1');

        array_walk($raw, function($k, $v) use(&$res) {
            $res[intval($k)] = substr($v, strrpos($v, '.')+1);
        });

        return $res;

    }


    /**
     * Retrieve all modulation profile list
     *
     * @return array
     */
    public function getModulationProfiles(): array
    {

        $res   = [];
        $table = [1 => 'other', 2 => 'qpsk', 3 => '16-qam', 4 => '8-qam', 5 => '32-qam', 6 => '64-qam', 7 => '128-qam'];
        $raw   = $this->walk('.1.3.6.1.2.1.10.127.1.3.5.1.4');

        array_walk($raw, function($v, $k) use(&$res, $table){
            
            $k  = explode('.', $k);     // разбираем индекс на части
            $v  = $table[intval($v)];   // тип используемой модуляции
            
            /** @noinspection PhpUnusedLocalVariableInspection */
            $k1 = array_pop($k);        // шаг построения профиля модуляции
            $k2 = array_pop($k);        // номер профиля
            $res[$k2] = $v;
            
        });

        return $res;

    }


    /**
     * Retrieve upstream channel frequency from docsIfUpChannelFrequency
     *
     * @param  string $index upstream channel ID
     * @return string
     */
    public function getFrequency(string $index): string
    {
        return $this->get(".1.3.6.1.2.1.10.127.1.1.2.1.2.$index");
    }


    /**
     * Retrieve upstream channel width from docsIfUpChannelWidth
     *
     * @param  string $index upstream channel ID
     * @return string
     */
    public function getWidth(string $index): string
    {
        return $this->get(".1.3.6.1.2.1.10.127.1.1.2.1.3.$index");
    }


    /**
     * Retrieve upstream channel modulation profile from docsIfUpChannelModulationProfile
     *
     * @param  string $index upstream channel ID
     * @return string
     */
    public function getModulationProfile(string $index): string
    {
        return $this->get(".1.3.6.1.2.1.10.127.1.1.2.1.4.$index");
    }


    /**
     * Retrieve upstream channel type from docsIfUpChannelType
     *
     * @param  string $index upstream channel ID
     * @return string
     */
    public function getType(string $index): string
    {

        $table = [1 => 'tdma', 2 => 'atdma', 3 => 'scdma'];
        $data  = $this->get(".1.3.6.1.2.1.10.127.1.1.2.1.15.$index");

        return array_key_exists($data, $table) ? $table[$data] : 'Unknown';

    }


    /**
     * Retrieve upstream channel alias from ifAlias
     *
     * @param  string $index upstream channel ID
     * @return string
     */
    public function getAlias(string $index): string
    {
        return $this->get(".1.3.6.1.2.1.31.1.1.1.18.$index");
    }


    /**
     * Retrieve upstream channel admin state from ifAdminStatus
     * 0: n/a
     * 1: up
     * 2: down
     *
     * @param  string $index upstream channel ID
     * @return int
     */
    public function getState(string $index): int
    {
        $state = $this->get(".1.3.6.1.2.1.2.2.1.7.$index");
        return intval(preg_replace('/[^\d]/', '', $state));
    }
    
}
