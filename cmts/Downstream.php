<?php

namespace dautkom\docsis\cmts;
use dautkom\docsis\Cmts;


/**
 * Methods for retrieving CMTS downstream channel params and settings 
 * @package dautkom\docsis\cmts
 */
class Downstream extends Cmts
{

    /**
     * Retrieve downstream frequency from docsIfDownChannelFrequency.
     *
     * @return string
     */
    public function getFrequency(): string 
    {
        return $this->get('.1.3.6.1.2.1.10.127.1.1.1.1.2.4');
    }

    /**
     * Retrieve downstream modulation from docsIfDownChannelModulation.
     *
     * @return string
     */
    public function getModulation(): string
    {

        $table = ['3' => '64-QAM', '4' => '256-QAM'];
        $data  = $this->get('.1.3.6.1.2.1.10.127.1.1.1.1.4.4');

        return array_key_exists($data, $table) ? $table[$data] : 'Unknown';

    }

    /**
     * Retrieve downstream channel width from docsIfDownChannelWidth.
     *
     * @return string
     */
    public function getWidth(): string
    {
        return $this->get('.1.3.6.1.2.1.10.127.1.1.1.1.3.4');
    }

    /**
     * Retrieve downstream power level adjustement from docsIfDownChannelPower.
     *
     * @return string
     */
    public function getPowerLevel(): string
    {
        return $this->get( '.1.3.6.1.2.1.10.127.1.1.1.1.6.4' );
    }
    
}
