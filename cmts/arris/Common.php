<?php

namespace dautkom\docsis\cmts\arris;
use dautkom\docsis\{
    cmts\Modems, cmts\Downstream, cmts\Upstream
};


/**
 * Common methods for Arris Cadant C3 CMTS
 * @package dautkom\docsis\cmts\arris
 */
class Common extends \dautkom\docsis\cmts\Common
{

    /**
     * @ignore
     */
    public function __construct()
    {
        $this->downstream = new Downstream();
        $this->upstream   = new Upstream();
        $this->modems     = new Modems();
    }

}
