# Docsis PHP library

PHP library for Docsis 2.0 cable modems and CMTS

## Requirements

* PHP 7.0 or higher
* php_snmp extension
* dautkom/netsnmp package

## License

Copyright (c) 2016 Olegs Capligins and respective contributors under the MIT License.

# Usage

Examples below cover general usage. Consider including necessary classes and dependencies.
 
## Basic usage for CMTS

    <?php 
    
    // Second argument with snmp configuration is optional and can be partial
    $cmts = (new dautkom\docsis\Cmts())->connect('10.0.0.10', ['public']);
    
    // Fetch necessary data
    echo $cmts->getContact();
    echo $cmts->getLocation();
    echo $cmts->getName();
    echo $cmts->getSysDescr();
    echo $cmts->getUptime();
    
    // Look for all available upstreams and modulation profiles
    $ups = $cmts->upstream->getUpstreams();
    $mpr = $cmts->upstream->getModulationProfiles()
    
    // Fetch necessary data from 11th upstream ID
    echo $cmts->upstream->getType(11);
    echo $cmts->upstream->getWidth(11);
    echo $cmts->upstream->getModulationProfile(11);
    echo $cmts->upstream->getFrequency(11);
    echo $cmts->upstream->getAlias(11);
    
    // List all cable modems and modems from particular upstream ID
    $all = $cmts->modems->getAll();
    $upm = $cmts->modems->getByUpstreamIndex(11);
    
    // Fetch downstream data
    echo $cmts->downstream->getModulation();
    echo $cmts->downstream->getWidth();
    echo $cmts->downstream->getFrequency();
    echo $cmts->downstream->getPowerLevel();

## Basic usage for cable modems

    <?php 
    
    /** @var \dautkom\docsis\modem\motorola\Common $modem */
    $modem = (new dautkom\docsis\Modem())->connect('172.16.16.70', ['public', 'private']);
    
    // Fetch common data
    echo $modem->getSysDescr();
    echo $modem->getUptime();
    echo $modem->getConfig();
    echo $modem->getSerialNumber();
    echo $modem->getStandby();
    
    // Fetch downstream data
    echo $modem->downstream->getChannelId();
    echo $modem->downstream->getFrequency();
    echo $modem->downstream->getMicroreflections();
    echo $modem->downstream->getPowerLevel();
    echo $modem->downstream->getSNR();
    
    // Fetch upstream data
    echo $modem->upstream->getChannelId();
    echo $modem->upstream->getPowerLevel();
    echo $modem->upstream->getFrequency();
    echo "\n";
    
    // Fetch RF (radiofrequency) data
    echo $modem->rf->getD2Mode();
    echo $modem->rf->getCMLostSyncAmount();
    echo $modem->rf->getCMRangingAbortAmount();
    echo $modem->rf->getCMResetAmount();
    $errors = $modem->rf->getErrors();
    
    // Fetch IP interface data
    $data   = $modem->ipif->getDataFlow();
    $errors = $modem->ipif->getErrors();
    $ports  = $modem->ipif->getPortState();
    echo $modem->ipif->getTFTPAccessAttempts();
    
    // Fetch FDB table from cable modem
    $fdb = $modem->fdb->getFdb();
