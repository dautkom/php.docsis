<?php

namespace dautkom\docsis;
use dautkom\netsnmp;


/**
 * Docsis CMTS library core class
 * @package dautkom\docsis
 */
class Cmts extends Core
{

    /**
     * Initialize properties for SNMP connection
     * 
     * @param  string $ip    SNMP agent IP-address
     * @param  array  $_snmp [optional] SNMP settings: communities and timeouts
     * @return Modem
     * @throws \RuntimeException
     */
    public function connect(string $ip, array $_snmp=[])
    {

        $ip = trim($ip);

        if( !filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
            throw new \RuntimeException( 'Wrong IP-address specified' );
        }

        self::$snmp = (new netsnmp\NetSNMP())->init($ip, $_snmp);

        // Get sysDescr
        try {
            $model = @self::$snmp->get('.1.3.6.1.2.1.1.1.0');
        }
        catch (\Throwable $t) {
            $model = false;
        }

        if( !$model ) {
            throw new \RuntimeException("Unable to connect to device $ip. Check if device is offline or has misconfigured SNMP settings.");
        }

        $match = [];
        self::$connected = true;

        /**
         * $match[1] - Vendor name, e.g. "Arris"
         * $match[2] - Model name, e.g. "C3"
         */
        preg_match('/Vendor:\s?([^\s;]+).+model:\s?([^>]+)/im', $model, $match);

        if( empty( $match ) || !array_key_exists('1', $match) || !array_key_exists('2', $match) ) {
            throw new \RuntimeException( "Unable to retrieve system description from $ip" );
        }
        else {
            $vendor = strtolower($match[1]);
            $model  = strtoupper($match[2]);
        }

        $vendor = __NAMESPACE__."\\cmts\\$vendor";
        $model  = str_replace($vendor, "$vendor\\$model", $vendor);

        // Return object
        try {
            $model = "$model\\Common";
            return new $model;
        }
        catch(\Throwable $t) {
            $vendor = "$vendor\\Common";
            return new $vendor;
        }
        
    }
    
}
