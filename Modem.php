<?php

namespace dautkom\docsis;
use dautkom\netsnmp;


/**
 * Docsis modem library core class
 * @package dautkom\docsis
 */
class Modem extends Core
{

    /**
     * Initialize properties for SNMP connection
     *
     * @param  string $ip     SNMP agent IP-address
     * @param  array  $_snmp  [optional] SNMP settings: communities and timeouts
     * @return Modem
     * @throws \RuntimeException
     */
    public function connect(string $ip, array $_snmp=[])
    {

        $ip = trim($ip);

        if( !filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
            throw new \RuntimeException( 'Wrong IP-address specified' );
        }

        // Instantiate SNMP engine
        self::$snmp = (new netsnmp\NetSNMP())->init($ip, $_snmp);

        // Get sysDescr
        try {
            $model = @self::$snmp->get('.1.3.6.1.2.1.1.1.0');
        }
        catch (\Throwable $t) {
            $model = false;
        }

        if( !$model ) {
            throw new \RuntimeException("Unable to connect to device $ip. Check if device is offline or has misconfigured SNMP settings.");
        }

        $match           = [];
        self::$connected = true;

        /**
         * $match[1] - Vendor name, e.g. "Motorola"
         * $match[2] - Model name, e.g. "SB5100E"
         */
        preg_match('/Vendor:\s?([^\s;]+).+model:\s?([^>]+)/im', $model, $match);

        if( empty( $match ) || !array_key_exists('1', $match) || !array_key_exists('2', $match) ) {
            throw new \RuntimeException( "Unable to retrieve system description from $ip" );
        }
        else {
            $vendor = strtolower($match[1]);
            $model  = strtoupper($match[2]);
        }

        $class  = __NAMESPACE__."\\modem\\$vendor";
        $model  = str_replace($vendor, "$vendor\\$model", $vendor);

        // Return object
        try {
            $model = "$model\\Common";
            return new $model;
        }
        catch(\Throwable $t) {
            try {
                $class = "$class\\Common";
                return new $class;
            }
            catch (\Throwable $e) {
                throw new \RuntimeException("Vendor $vendor is not supported");
            }
        }

    }


    /**
     * Ini library loading
     *
     * @throws \Exception
     * @return void
     */
    protected function loadIni()
    {

        // Who called me?
        $invoker = new \ReflectionClass($this);
        $invoker = $invoker->getNamespaceName();

        // Get path for ini file
        $ini = __DIR__ . str_replace([ __NAMESPACE__, '\\'], ['', DIRECTORY_SEPARATOR], $invoker) . DIRECTORY_SEPARATOR . 'model.ini';

        if( !file_exists( $ini ) ) {
            throw new \Exception( "$ini not found" );
        }

        self::$device = parse_ini_file($ini);
        self::validateIni();

    }


    /**
     * Validate params loaded from ini file
     *
     * @throws \Exception
     * @return void
     */
    private static function validateIni()
    {
        if( empty(self::$device) ) {
            throw new \Exception('Empty or invalid model.ini file');
        }
    }

}
